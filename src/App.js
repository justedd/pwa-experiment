import React, { Component } from 'react'
import camera from './assets/camera.png'
import './App.css'
import 'bootstrap/dist/css/bootstrap.css'

class App extends Component {

  constructor(props) {
    super(props)

    this.theStream = null
    this.theRecorder = null
    this.recordedChunks = []
  }

  getUserMedia = (options, successCallback, failureCallback) => {
    var api = navigator.getUserMedia || navigator.webkitGetUserMedia ||
      navigator.mozGetUserMedia || navigator.msGetUserMedia
    if (api) {
      return api.bind(navigator)(options, successCallback, failureCallback)
    }
  }

  recorderOnDataAvailable = (event) => {
    if (event.data.size === 0) return
    this.recordedChunks.push(event.data)
  }

  getStream = () => {
    if (!navigator.getUserMedia && !navigator.webkitGetUserMedia &&
      !navigator.mozGetUserMedia && !navigator.msGetUserMedia) {
      alert('User Media API not supported.')
      return
    }

    var constraints = { video: true, audio: true }
    this.getUserMedia(constraints, (stream) => {
      var mediaControl = document.querySelector('video')

      if ('srcObject' in mediaControl) {
        mediaControl.srcObject = stream
        mediaControl.src = (window.URL || window.webkitURL).createObjectURL(stream)
      } else if (navigator.mozGetUserMedia) {
        mediaControl.mozSrcObject = stream
      }

      this.theStream = stream
      try {
        this.recorder = new MediaRecorder(stream, { mimeType: "video/webm" })
      } catch (e) {
        console.error('Exception while creating MediaRecorder: ' + e)
        return
      }

      this.theRecorder = this.recorder
      console.log('MediaRecorder created')

      this.recorder.ondataavailable = this.recorderOnDataAvailable
      this.recorder.start(100)
    }, function (err) {
      alert('Error: ' + err)
    })
  }

  download = () => {
    console.log('Saving data')
    this.theRecorder.stop()
    this.theStream.getTracks()[0].stop()

    var blob = new Blob(this.recordedChunks, { type: "video/webm" })
    var url = (window.URL || window.webkitURL).createObjectURL(blob)
    var a = document.createElement("a")
    document.body.appendChild(a)
    a.style = "display: none"
    a.href = url
    a.download = 'test.webm'
    a.click()

    setTimeout(function () {
      (window.URL || window.webkitURL).revokeObjectURL(url)
    }, 100)
  }

  render = () => {
    return (
      <div className="App">
        <header className="App-header">
          <video className="mb-3" autoPlay muted style={{ maxHeight: '480px', width: '100%' }} poster={camera}></video>
          <p><button className="btn btn-lg btn-default" onClick={this.getStream}>Захват видео и запись</button></p>
          <p><button className="btn btn-lg btn-default" onClick={this.download}>Сохранить</button></p>
        </header>
      </div>
    );
  }
}

export default App
